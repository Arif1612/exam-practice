<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Organic Food</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Font Awesome Kit -->
    <script src="https://kit.fontawesome.com/496c26838e.js" crossorigin="anonymous"></script>

    <style>
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top border-bottom">
        <div class="container">
            <div class="me-0">
                <!-- Brand image -->
                <a class="navbar-brand" href="index.html">
                    <img src="resources/images/logo.png" alt="Organic Food" height="56px">
                </a>
            </div>
            <!-- Collapseable nav bar options-->
            <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active text-green" aria-current="page" href="index.html">Home</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            Categories
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            @foreach ($categories as $category)
                                <li><a class="dropdown-item" href="vegetables.html">{{ $category->name }} </a></li>
                            @endforeach

                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="categoryList.html">Products</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="aboutUs.html">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contactUs.html">Contact Us</a>
                    </li>
                </ul>
            </div>

            <!-- Cart, favourites and nav bar toggler button -->
            <div class="justify-content-center">
                <div class="d-flex justify-content-center pt-3">

                    <a href="favourites.html" class="btn btn-outline-primary position-relative ms-2 me-3">
                        <i class="fa-solid fa-heart"></i>
                        <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                            99+
                            <span class="visually-hidden">Items</span>
                        </span>
                    </a>

                    <a href="cart.html" class="btn btn-outline-primary position-relative ms-3 me-2">
                        <i class="fa fa-cart-shopping"></i>
                        <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                            99+
                            <span class="visually-hidden">Items</span>
                        </span>
                    </a>

                    <button class="navbar-toggler ms-3 me-2" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
            </div>
        </div>
    </nav>

    <!-- Main section/content -->
    <section class="mt-2 mx-md-5">
        <div class="container-md">
            <div class="row">
                <div class="col-md-12 py-3">
                    <!-- Search products by categories -->
                    <div class="row justify-content-center mb-3">
                        <form class="col-md-12" action="" method="get">
                            <div class="input-group mb-3 justify-content-center">
                                <div class="col-md-3 mx-md-2">
                                    <select class="form-select form-control bg-green" name="categories">
                                        <option value="0" selected>All Categories</option>
                                        <option value="1">Vegetables</option>
                                        <option value="2">Fruits</option>
                                        <option value="3">Drinks</option>
                                        <option value="4">Ingridients</option>
                                    </select>
                                </div>

                                <div class="col-md-4 mx-md-2">
                                    <input type="text" class="form-control" placeholder="Search product"
                                        name="search" aria-label="Search product name" aria-describedby="search-all">
                                </div>
                                <div class="col-md-1 mx-md-2">
                                    <button class="btn bg-green" type="submit" id="search-all">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <!-- Landing image and text -->
                    <div class="row position-relative d-flex">
                        <img class="img-fluid" src="resources/images/home.png" alt="">
                        <!-- Absolute positioned text -->
                        <div
                            class="home-text position-absolute d-flex flex-sm-column top-0 end-0 bottom-0 text-end text-white m-sm-auto pe-5 justify-content-center
						initialism fs-4 letter-spacing-1">
                            <div>
                                <p class="letter-spacing-2">Fresh Food</p>
                            </div>
                            <div>
                                <p class="fs-2"><b>Fresh Vegetable</b></p>
                            </div>
                            <div>
                                <p class="fs-2"><b>100% Organic</b></p>
                            </div>
                            <div>
                                <p class="fs-5">Free Delivery</p>
                            </div>
                            <div>
                                <a href="products.html" class="btn bg-green btn-styled">Shop now</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="col-md-3 py-md-3">
     <div class="accordion" id="accordionExample">
      <div class="accordion-item">
       <h2 class="accordion-header" id="headingOne">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne"
         aria-expanded="true" aria-controls="collapseOne">
         All categories
        </button>
       </h2>
       <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
        data-bs-parent="#accordionExample">
        <div class="accordion-body p-0">
         <div class="list-group-flush m-md-2">
          <a href="#" class="list-group-item list-group-item-action border-0 p-md-3">Vegetables</a>
          <a href="#" class="list-group-item list-group-item-action border-0 p-md-3">Fruits</a>
          <a href="#" class="list-group-item list-group-item-action border-0 p-md-3">Drinks</a>
          <a href="#" class="list-group-item list-group-item-action border-0 p-md-3">Ingridients</a>
         </div>
        </div>
       </div>
      </div>
     </div>
    </div> -->
            </div>
        </div>
    </section>

    <!-- Fruite Details  -->
    <div class="container">
        <br>
        <!-- Banana -->

        <div class="row row-cols-1 row-cols-md-3  g-4">
            @foreach ($products as $product)
                <div class="col h-100">
                    <div class="card ">
                        <img src="" alt="{{ $product->name }}">
                        <div class="card-body btn-light text-center ">
                            <p>{{ $product->name }} </p>
                            <p>Price {{ $product->price }} ৳</p>
                            <p>Description {{ $product->description }}</p>
                            <a href=""> <button type="button" class="btn btn-danger ">Add
                                    To
                                    Cart</button></a>
                            <a class="btn btn-primary" href=""> Show Details </a>

                        </div>
                    </div>
                </div>
            @endforeach


        </div>

        <!-- Mango End -->
    </div>

    <footer class="container-fluid bg-light border-top mt-5 pt-5">
        <div class="container">
            <!-- Top footer -->
            <div class="row border-bottom pb-4 ms-5 me-5">
                <div class="col-md-4">
                    <h5 class="text-green">
                        <img src="resources/images/logo.png" alt="Organic Food" height="64px">
                        Organic Food
                    </h5>
                    <div class="ps-3 pe-4">
                        <p><a href="https://goo.gl/maps/vBBH9KKdM6UKfMTh9">Address: Uttara, Sector#7, Dhaka-1230</a>
                        </p>
                        <p><a href="tel:+8801444444444">Phone: +8801444444444</a></p>
                        <p><a href="mailto:info.organic.food@gmail.com">Email: info.organic.food@gmail.com</a></p>
                    </div>
                </div>
                <div class="col-md-4 pt-4">
                    <h5>Useful links</h5>
                    <div class="pe-4 pt-3">
                        <p><a href="aboutUs.html">About Us</a></p>
                        <p><a href="contactUs.html">Contact Us</a></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <h5>Join Our Newsletter Now</h5>
                    <p class="small">Get E-mail updates about our latest shop and special offers</p>
                    <div class="pt-2 pb-2">
                        <div class="row">
                            <form action="" method="get">
                                <div class="input-group mb-3">
                                    <div class="col-md-7">
                                        <input type="email" class="form-control" placeholder="Enter your email"
                                            name="subcribe-email" aria-label="Enter your email"
                                            aria-describedby="subscribe">
                                    </div>
                                    <div class="col-md-1">
                                        <button class="btn bg-green" type="submit" id="subscribe">Subscribe</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="social-links pt-2">
                        <a class="shadow" style="height: 32px;" href="https://www.facebook.com/organic-food/"
                            target="_blank" rel="noopener" title="Facebook">
                            <i class="fa fa-facebook-f"></i>
                        </a>
                        <a class="shadow" href="https://www.instagram.com/organic-food/" target="_blank"
                            rel="noopener" title="Instagram">
                            <i class="fa fa-instagram"></i>
                        </a>
                        <a class="shadow" href="https://www.youtube.com/channel/organic-food" target="_blank"
                            rel="noopener" title="Youtube">
                            <i class="fa fa-youtube"></i>
                        </a>
                    </div>
                </div>
            </div>

            <!-- Bottom footer -->
            <div class="row pb-4 pt-4 ms-5 me-5">
                <p class="pt-2 text-center">Copyright &copy;2022 All rights reserved <b>Batch-007</b></p>
            </div>
        </div>
    </footer>

    <!-- Bootstrap bundle -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>

    <!-- jQuery -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
</body>

</html>
