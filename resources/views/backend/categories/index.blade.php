<x-backend.master>
    <x-slot:title>
        Categoiries List
    </x-slot:title>

    <main id="main" class="main">

        <x-forms.message />

        {{-- @if (session('message'))
            <span class="text-success">{{ session('message') }} </span>
        @endif --}}

        {{-- sir ar theke uporer part ta  --}}
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Categories</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group me-2">
                    <a href="{{ route('categories.pdf') }}">
                        <button type="button" class="btn btn-sm btn-outline-success">PDF</button>
                    </a>
                    <button type="button" class="btn btn-sm btn-outline-secondary">Excel</button>
                    <a href="{{ route('categories.trash') }}">
                        <button type="button" class="btn btn-sm btn-outline-danger">Trash</button>
                    </a>
                </div>
                <a href="{{ route('categories.create') }}">
                    <button type="button" class="btn btn-sm btn-outline-primary">
                        <span data-feather="plus"></span>
                        Add New
                    </button>
                </a>
            </div>
        </div>

        {{-- <a href="{{ route('categories.create') }} ">Create</a> --}}
        <table class="table">
            <thead>
                <tr>
                    <th>SL#</th>
                    <th>Category Name </th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($categories as $category)
                    <tr>
                        <td>{{ $loop->iteration }} </td>
                        <td>{{ $category->name }} </td>
                        <td>
                            <a class="btn btn-success" href="{{ route('categories.show', $category->id) }} ">Show
                            </a> |
                            <a class="btn btn-warning" href="{{ route('categories.edit', $category->id) }} ">Edit</a> |
                            <form action="{{ route('categories.destroy', $category->id) }}" method="post"
                                style="display:inline">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger" href=""
                                    onclick="return confirm('Are You Sure?')">Delete</button>
                            </form>

                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </main>
</x-backend.master>
