<x-backend.master>
    <x-slot:title>
        Category Update
        </x-slot>
        <main id="main" class="main">
            {{-- sir ar theke uporer part ta  --}}
            <div
                class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Categories</h1>
                <div class="btn-group me-2">
                    {{-- class="btn-toolbar mb-2 mb-md-0" --}}

                    <a href="{{ route('categories.index') }}">
                        <button type="button" class="btn btn-sm btn-outline-info">
                            <span data-feather="List"></span>
                            List
                        </button>
                    </a>
                </div>
            </div>

            {{-- validation Check message --}}
            <x-forms.errors />

            {{-- @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif --}}
            {{-- Form dite hobe --}}
            <form action="{{ route('categories.update', $category->id) }} " method="post" enctype="multipart/form-data">
                @csrf
                @method('patch')
                <x-forms.input type="text" name="name" :value="old('name', $category->name)" placeholder="Enter Category Name"
                    required />
                {{-- <div class="mb-3">
                    <label for="nameInput" class="form-label ">Name</label>
                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                        id="exampleInputEmail1" value="{{ $category->name }} ">
                    @error('name')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div> --}}

                <img src="{{ asset('storage/categories/' . $category->image) }}" height="250" />

                <x-forms.input type="file" name="image" label='Picture' />

                @php
                    $checklist = [
                    "Is_Active"=> 'is_active'
                  ];

                    if ($category->is_active) {
                        $checkedItems = [0];
                    } else {
                        $checkedItems = [];
                    }
                @endphp

                <x-forms.checkbox :checklist="$checklist" :checkedItems="$checkedItems" name="is_active" />

                <button type="submit" class="btn btn-primary">Submit</button>

                {{-- <input type="file" name="image" id="image">

                <div class="mb-3 form-check">
                    <input name="is_active" type="checkbox" class="form-check-input" id="exampleCheck1"
                        @if ($category->is_active) checked @endif>
                    <label class="form-check-label" for="isActiveInput">Is Active ?</label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button> --}}
            </form>
        </main>




</x-backend.master>
