<x-backend.master>
    <x-slot:title>
        Product Details
        </x-slot>
        <main class="main" id="main">
            <div
                class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Product</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <a href="{{ route('products.index') }}">
                        <button type="button" class="btn btn-sm btn-outline-primary">
                            <span data-feather="list"></span>
                            List
                        </button>
                    </a>
                </div>
            </div>

            <p>Category: {{ $product->category?->name }}</p>
            <h1>Product Name: {{ $product->name }}</h1>
            <p>Price:{{ $product->price }}</p>
            <p>Description:{{ $product->description }}</p>
            <img src="{{ asset('storage/products/' . $product->image) }}" />
            {{-- <p>Colors:
            <ol>
                @foreach ($product->colors as $color)
                    <li>{{ $color->title }}</li>
                @endforeach
            </ol>
            </p> --}}
            <p>Is Active ?: {{ $product->is_active ? 'Yes' : 'No' }}</p>
        </main>
</x-backend.master>
