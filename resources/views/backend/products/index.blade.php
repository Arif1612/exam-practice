<x-backend.master>
    <x-slot:title>
        Product List
        </x-slot>

        <main class="main" id="main">
            <x-forms.message />

            <div
                class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Products</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group me-2">
                        <a href="{{ route('products.pdf') }}">
                            <button type="button" class="btn btn-sm btn-outline-secondary">PDF</button>
                        </a>
                        <button type="button" class="btn btn-sm btn-outline-secondary">Excel</button>
                        <a href="{{ route('products.trash') }}">
                            <button type="button" class="btn btn-sm btn-outline-danger">Trash</button>
                        </a>
                    </div>
                    <a href="{{ route('products.create') }}">
                        <button type="button" class="btn btn-sm btn-outline-primary">
                            <span data-feather="plus"></span>
                            Add New
                        </button>
                    </a>
                </div>
            </div>

            <table class="table">
                <thead>
                    <tr>
                        <th>SL#</th>
                        <th>Product Name</th>
                        <th width="180">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $product->name }}</td>
                            <td>
                                <a class="btn btn-sm btn-info"
                                    href="{{ route('products.show', $product->id) }}">Show</a>
                                <a class="btn btn-sm btn-warning"
                                    href="{{ route('products.edit', $product->id) }}">Edit</a>
                                <form action="{{ route('products.destroy', $product->id) }}" method="post"
                                    style="display:inline">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-sm btn-danger"
                                        onclick="return confirm('Are you sure want to delete')">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $products->links() }}

        </main>
</x-backend.master>
