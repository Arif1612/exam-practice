<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/',[FrontendController::class, 'index'])->name('frontend.index');

require __DIR__.'/auth.php';

Route::middleware('auth')->group(function(){

Route::controller(CategoryController::class)->group(function() {
    Route::prefix('categories')->name('categories.')->group(function() {
        Route::get('/pdf', 'downloadPdf')->name('pdf');
        Route::get('/excel', 'downloadExcel')->name('excel');

        // Soft delete
        Route::get('/trash', 'trash')->name('trash');
        Route::get('/trash/{id}/view', 'trashedShow')->name('trash.show');
        Route::get('/{id}/restore', 'restore')->name('restore');
        Route::delete('/{id}/delete', 'delete')->name('delete');
    });
});
Route::resource('categories', CategoryController::class);

Route::controller(ProductController::class)->group(function() {
    Route::prefix('products')->name('products.')->group(function() {
        Route::get('/pdf', 'downloadPdf')->name('pdf');
        Route::get('/excel', 'downloadExcel')->name('excel');

        Route::get('/pdf-trashed', 'downloadTrashedPdf')->name('pdf.trashed');
        Route::get('/excel-trashed', 'downloadTrashedExcel')->name('excel.trashed');

        // Soft delete
        Route::get('/trash', 'trash')->name('trash');
        Route::get('/trash/{id}/view', 'trashedShow')->name('trash.show');
        Route::get('/{id}/restore', 'restore')->name('restore');
        Route::delete('/{id}/delete', 'delete')->name('delete');
    });
});
Route::resource('products', ProductController::class);

});

