<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Image;
use Barryvdh\DomPDF\Facade\Pdf;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('backend.categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $requestData = [
            'name'=>$request->name,
            'is_active'=>$request->is_active ? true:false,
            'image' => $this->uploadImage($request->file('image'))

        ];
        Category::create($requestData);
        return redirect()
        ->route('categories.index')
        ->withMessage('Successfully Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('backend.categories.show',compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('backend.categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request,Category $category)
    {
        $requestData = [
            'name'=>$request->name,
            'is_active'=>$request->is_active ? true:false,
            // 'image'=>$imgFileName
        ];

        if ($request->hasFile('image')) {
            $requestData['image'] = $this->uploadImage($request->file('image'));
        }

        $category->update($requestData);
        // Category::Create($requestData);
        return redirect()
        ->route('categories.index')
        ->withMessage('Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return redirect()
        ->route('categories.index')
        ->withMessage('Successfully Deleted');
    }

    public function uploadImage($image)
    {
        $originalName = $image->getClientOriginalName();
        $fileName = date('Y-m-d') . time() . $originalName;

        // $image->move(storage_path('/app/public/categories'), $fileName);

        Image::make($image)
            ->resize(200, 200)
            ->save(storage_path() . '/app/public/categories/' . $fileName);

        return $fileName;
    }

    public function trash()
    {
        $categories = Category::onlyTrashed ()->get();
        return view('backend.categories.trash',compact('categories'));
    }

    public function delete($id)
    {
        $category = Category::onlyTrashed()->find($id);
        $category->forceDelete();
        return redirect()
        ->route('categories.trash')
        ->withMessage('Successfully Deleted');
        // return view('backend.categories.trash',compact('categories'));
    }
    public function restore($id)
    {
        $category = Category::onlyTrashed()->find($id);
        $category->restore();
        return redirect()
        ->route('categories.trash')
        ->withMessage('Successfully Restore');
    }

    public function downloadPdf()
    {
        $categories = Category::all();
        $pdf = Pdf::loadView('backend.categories.pdf', compact('categories'));
        return $pdf->download('category-list.pdf');
    }
}
